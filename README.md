<!-- Hero -->
H1 - Hero Title
Paragraph - Paragraph Tag
Link * 3
CTA - Services
Hero Video

<!-- Small Hero -->
H1 - Hero Title
P - Paragraph Tag

<!-- Quote CTA -->
H2
Paragraph - 1 sentence
Button

<!-- Large Quote CTA -->
H2
Paragraph - 1 Sentence
Button

<!-- Newsletter CTA -->
H2
Paragraph - 1 sentence
Input 
Button

<!-- Featured Products -->
Img
Product Name
Price

<!-- Testimonials Section -->
Paragraph
Name
Company

<!-- Single Service -->
Image
Service Name
CTA Link

<!-- Partner CTA -->
Tagline
H2
Paragraph
Logo

