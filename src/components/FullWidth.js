import React, {Component} from 'react';

class FullWidth extends Component {
    render(){
        return(
            <div className="container">
                <p>
                    {this.props.children}
                </p>
            </div>
        );
    }
}

export default FullWidth;