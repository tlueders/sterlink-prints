import React, {Component} from 'react';
import '../styles/SubscribeHalf.scss';
import SubscribeInput from './SubscribeInput';

class SubscribeHalf extends Component {
    render() {
        return(
            <section className="subscribeCta">
                <div className="subscribeContent">
                    <h2>Subscribe to Our Newsletter!</h2>
                    <p>Sign up for Sterlink Prints products and service updates!</p>
                    <SubscribeInput />
                </div>
            </section>
        );
    }
}

export default SubscribeHalf; 