import React, { Component } from 'react';
import '../styles/Garments.scss';

import alphaLogo from '../images/alphabroder-logo.png';

class TwoColumnContentRev extends Component {
    render() {
    return (
        <section className="container">
            <div className="content-container">
            <div className="content-box">
                <div className="content-box-content">
                    <h5>Order Blank Shirts from Alphabroder</h5>
                    <h2>Alphabroder</h2>
                    <p>
                    {this.props.description}
                    </p>
                    <img className="partner-logo" src={alphaLogo} alt=""/>
                </div>
            </div>
            <div className="image-box">
                    <div className="image-box-content">
                        <h5 className="brand-box-tagline">Alphabroder - Great Garment Selection</h5>
                        <button><a href="https://www.alphabroder.com">+</a></button>
                    </div>
                </div>
            </div>
        </section>
      );
    }
  }
  
  export default TwoColumnContentRev;