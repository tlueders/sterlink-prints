import React, { Component } from 'react';
import '../App.scss';
import axios from 'axios';

// Components
import Hero from './Hero';
import QuoteHalf from './QuoteHalf';
import SubscribeHalf from './SubscribeHalf';
import Reviews from './Reviews';
import InstafeedLayout from './Instafeed';
import Navigation from './Navigation';
import Footer from './Footer';

import Loader from './Loader';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      homepage: {}
    }
  }

  componentDidMount() {
    axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/146')
      .then(res => {
        const content = res.data.acf;
        this.setState({
          homepage: content
        })
      })
      .catch(err => {
        console.log(err);
      })
  }
 
  render() {
    return (
      
      <div className="App">
        {
          Object.keys(this.state.homepage).length === 0 ?
          <Loader /> :
          <div>
          <Navigation />
          <Hero heroTitle={this.state.homepage.heroTitle} heroCaption={this.state.homepage.heroCaption}/>
          <div className="container">         
            <section className="ctaSection">
              <QuoteHalf />
              <SubscribeHalf />
            </section>
          </div>
          <Reviews 
            title={this.state.homepage.about_us_title} 
            about_description={this.state.homepage.about_us_description} 
            icon_title_one={this.state.homepage.icon_title_1} 
            icon_text_one={this.state.homepage.icon_text_1} 
            icon_title_two={this.state.homepage.icon_title_2} 
            icon_text_two={this.state.homepage.icon_text_2}
            icon_title_three={this.state.homepage.icon_title_3} 
            icon_text_three={this.state.homepage.icon_text_3}  
          />
          <InstafeedLayout />
          <Footer />
          </div>
        }
      </div>
    );
  }
}

export default App;
