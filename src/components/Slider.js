import React, {Component} from 'react';
import '../styles/Carousel.scss';

// MENS
import tshirtGood from '../images/options/PC54_good.jpg';
import tshirtBetter from '../images/options/DM130_better.jpg';
import tshirtBest from '../images/options/A204_best.jpg';

// WOMENS
import wtshirtGood from '../images/options/LPC381V_good.jpg';
import wtshirtBetter from '../images/options/DM130L_ BETTER.jpg';
import wtshirtBest from '../images/options/A208BEST.jpg';

// SWEATSHIRTS
import hoodieGood from '../images/options/PC90H_ good.jpg';
import hoodieBetter from '../images/options/s700_Better.jpg';
import hoodieBest from '../images/options/IND40_ best.jpg';

import Slider from "react-slick";
    function SampleNextArrow(props) {
        const { className, style, onClick } = props;
        return (
        <div
            className={className}
            style={{ ...style, display: "block"}}
            onClick={onClick}
        />
        );
    }
    
    function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
        <div
            className={className}
            style={{ ...style, display: "block"}}
            onClick={onClick}
        />
        );
    }

class OptionsSlider extends Component {  

    constructor(props) {
        super(props);

        this.state = {
            showTshirts:  true,
            showHoodies: false,
            showLadies: false,
        };

        this.showTshirts = this.showTshirts.bind(this);
        this.showHoodies = this.showHoodies.bind(this);
        this.showLadies = this.showLadies.bind(this);
    }

    showTshirts() {
        this.setState(state => ({
            showTshirts: true,
            showHoodies: false,
            showLadies: false,
        }));
    }   

    showHoodies() {
        this.setState(state => ({
            showTshirts: false,
            showHoodies: true,
            showLadies: false,
        }));
    }  
    
    showLadies() {
        this.setState(state => ({
            showTshirts: false,
            showHoodies: false,
            showLadies: true,
        }));
    }   

    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
          };
        return(
                <div>
                <div className="slide-options">
                    <ul>
                        <li><a onClick={this.showTshirts}>T-Shirts</a></li>
                        <li><a onClick={this.showHoodies}>Hoodies</a></li>
                        <li><a onClick={this.showLadies}>Ladies T-Shirts</a></li>
                    </ul> 
                </div>

                {this.state.showTshirts && 
                    <Slider {...settings}>
                        <div className={this.state.showTshirts && 'showTshirts' && 'slide'}>
                            <div className="product">
                                <img src={tshirtGood} alt=""/>
                            </div>
                            <div className="content">
                                <h2>PC54  Port &amp; Company Cotton Tee</h2>
                                <p>
                                    With 44 color ways and a classic fitting silhouette this is a great option for low budget events or work shirt.
                                </p>
                            </div>
                        </div>
                        <div className="slide">
                            <div className="product">
                                <img src={tshirtBetter} alt=""/>
                            </div>
                            <div className="content">
                                <h2>DM130 District  Perfect Tri  Tee</h2>
                                <p>
                                    Oh, so soft!  A premium soft shirt at about half of the cost of similar feeling shirts.
                                </p>
                            </div>
                        </div>
                        <div className="slide">
                            <div className="product">
                                <img src={tshirtBest} alt=""/>
                            </div>
                            <div className="content">
                                <h2>Allmade’s tri-blend T-shirt</h2>
                                <p>
                                    A wonderful fitting premium shirt that prints exceptionally well. Best feature, this shirt is made from recycled material!
                                </p>
                            </div>
                        </div>
                    </Slider>
                }

                {this.state.showHoodies && 
                    <Slider {...settings}>
                        <div className={this.state.showHoodies && 'showHoodies' && 'slide'}>
                            <div className="product">
                                <img src={hoodieGood} alt=""/>
                            </div>
                            <div className="content">
                                <h2>PC90H Port &amp; Company Fleece Sweatshirt</h2>
                                <p>
                                    Our 9-ounce favorite at a budget friendly price. A popular product for sports teams, events and work wear.
                                </p>
                            </div>
                        </div>
                        <div className="slide">
                            <div className="product">
                                <img src={hoodieBetter} alt=""/>
                            </div>
                            <div className="content">
                                <h2>Champion Dry Eco® Pullover Hood</h2>
                                <p>
                                    A great name brand hoodie at half the cost you are used to seeing in the stores!
                                </p>
                            </div>
                        </div>

                        <div className="slide">
                            <div className="product">
                                <img src={hoodieBest} alt=""/>
                            </div>
                            <div className="content">
                                <h2>Independent Heavyweight Hooded Pullover Sweatshirt</h2>
                                <p>
                                    Top quality heavy weight hoodie from the world’s favorite hoodie supplier.  Over 30 color ways!
                                </p>
                            </div>
                        </div>
                    </Slider>
                }

                {this.state.showLadies && 
                    <Slider {...settings}>
                        <div className={this.state.showLadies && 'showLadies' && 'slide'}>
                            <div className="product">
                                <img src={wtshirtGood} alt=""/>
                            </div>
                            <div className="content">
                                <h2>LPC381V Port &amp; Company Performance Blend Tee</h2>
                                <p>
                                    A soft cotton hand feel joins Dry Zone moisture-wicking technology for unbeatable comfort and performance. Budget friendly!
                                </p>
                            </div>
                        </div>

                        <div className={this.state.showLadies && 'showLadies' && 'slide'}>
                            <div className="product">
                                <img src={wtshirtBetter} alt=""/>
                            </div>
                            <div className="content">
                                <h2>DM130L District Perfect Tri Tee</h2>
                                <p>
                                    The perfect tee crafted from three yarns for carefree style. Will soon be one of your “comfy shirts”!
                                </p>
                            </div>
                        </div>

                        <div className={this.state.showLadies && 'showLadies' && 'slide'}>
                            <div className="product">
                                <img src={wtshirtBest} alt=""/>
                            </div>
                            <div className="content">
                                <h2>Allmade’s Womens Tri-blend T-shirt</h2>
                                <p>
                                    A wonderful fitting premium shirt that prints exceptionally well. Best feature, this shirt is made from recycled material!
                                </p>
                            </div>
                        </div>
                    </Slider>
                }

                </div>
        );
    }
}

export default OptionsSlider; 