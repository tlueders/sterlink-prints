import React, { Component } from 'react';
import axios from 'axios';
import '../App.scss';
import Loader from './Loader';

// Components
import Navigation from './Navigation';
import Footer from './Footer';
import HeroSmall from './HeroSmall';
import QuoteLarge from './QuoteLarge';
import ServiceList from './ServiceList';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      services_printing: {}
    }
  }

  componentDidMount() {
    axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/212')
      .then(res => {
        const content = res.data.acf;
        this.setState({
          services_printing: content
        })
        console.log(res.data.acf);
      })
      .catch(err => {
        console.log(err);
      })
  }

  render() {
    return (
      <div className="App">
      {
          Object.keys(this.state.services_printing).length === 0 ?
          <Loader /> :
          <div>
            <Navigation />
            <HeroSmall title={this.state.services_printing.page_title}>{this.state.services_printing.page_description}</HeroSmall>
            <ServiceList />
            <QuoteLarge />
            <Footer />
          </div>
      }
      </div>
    );
  }
}

export default App;
