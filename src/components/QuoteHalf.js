import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

import '../styles/QuoteHalf.scss';

class QuoteHalf extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          quote: {}
        }
    }

    componentDidMount() {
        axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/174')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              quote: content
            })
          })
          .catch(err => {
            console.log(err);
          })
    }

    render() {
        return(
            <section className="quoteCta">
                <div className="quoteContent">
                    <h2>{this.state.quote.cta_heading}</h2>
                    <p>{this.state.quote.cta_description_text}</p>
                    <Link to={"/quote"}><button className="secondary-btn">Get In Touch!</button></Link>
                </div>
            </section>
        );
    }
}

export default QuoteHalf; 