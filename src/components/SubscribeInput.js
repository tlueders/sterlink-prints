import React, {Component} from 'react';
import '../styles/SubscribeHalf.scss';
import MailchimpSubscribe from "react-mailchimp-subscribe";

const url = "https://sterlinkprints.us7.list-manage.com/subscribe/post?u=2bb3bdf49a780e503ba126120&amp;id=77035fe65a";

class SubscribeInput extends Component {
    render() {
        return(
            <div className="email-input">
               <MailchimpSubscribe url={url}/>
            </div>
        );
    }
}

// // simplest form (only email)
// const SimpleForm = () => 

export default SubscribeInput; 
// export default SimpleForm; 