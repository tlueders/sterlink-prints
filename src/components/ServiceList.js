import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../styles/Service.scss';
import LoaderSmall from './LoaderSmall';

import PrintingImage from '../images/printing-service.jpg';
import SportsBallImage from '../images/sportsball-service.jpg';

class ServiceList extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          services_printing: {},
          services_embroidery: {},
          services_balls: {}
        }
      }
    
      componentDidMount() {
        axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/180')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              services_printing: content
            })
          })
          .catch(err => {
            console.log(err);
          })
          axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/189')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              services_embroidery: content
            })
          })
          .catch(err => {
            console.log(err);
          })
          axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/194')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              services_balls: content
            })
          })
          .catch(err => {
            console.log(err);
          })
      }

  render() {

    return (
        <div>
            {
            Object.keys(this.state.services_printing || this.state.services_embroidery || this.state.services_balls).length === 0 ?
            <LoaderSmall /> :
            <div className="container services">
              <div className="single-service">
                  <div className="single-service-image"><img src={PrintingImage} alt=""/></div>
                  <div className="service-content">
                      <h3>{this.state.services_printing.service_title}</h3>
                      <Link to="/printing"><button type="button">See Details</button></Link> 
                  </div>
              </div>

              <div className="single-service">
              <div className="single-service-image"><img src='./images/serviceImage.jpg' alt=""/></div>
                  <div className="service-content">
                      <h3>{this.state.services_embroidery.service_title}</h3>
                      <Link to="/embroidery"><button type="button">See Details</button></Link> 
                  </div>
              </div>

              <div className="single-service">
                  <div className="single-service-image"><img src={SportsBallImage} alt=""/></div>
                    <div className="service-content">
                        <h3>{this.state.services_balls.service_title}</h3>
                        <Link to="/balls"><button type="button">See Details</button></Link> 
                    </div>
                  </div>
              </div>
            }
        </div>
    );
  }
}

export default ServiceList;
