import React, {Component} from 'react';
import '../styles/FeaturedBrands.scss';

import Independent from '../images/indpendent-logo.png';
import Bella from '../images/bella-canvas-logo.svg';
import NextLevel from '../images/nextlevel-logo.jpg';
import Richardson from '../images/richardson-logo.gif';
import PH from '../images/ph-logo.svg';


class FeaturedBrands extends Component {
    render() {
        return(
            <section className="featuredBrands">
                <h2>Featured Brands</h2>
                <div className="brands">
                    <ul>
                        <li><img src={Independent} alt=""/></li>
                        <li><img src={Bella} alt=""/></li>
                        <li><img src={PH} alt=""/></li>
                        <li><img src={NextLevel} alt=""/></li>
                        <li><img src={Richardson} alt=""/></li>
                    </ul>
                </div>
            </section>
        );
    }
}

export default FeaturedBrands;