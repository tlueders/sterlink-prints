import React, { Component } from 'react';
import '../styles/HeroSmall.scss';

class HeroSmall extends Component {
    render() {
      return (
        <section className="heroSectionSmall">
            <div className="container">
                <div className="heroContent">
                    <h1>{this.props.title}</h1>
                    <p>{this.props.children}</p>
                </div>
            </div>  
        </section>
      );
    }
  }
  
  export default HeroSmall;