import React, {Component} from 'react';
import '../styles/QuoteLarge.scss';
import axios from 'axios';

import {Link} from 'react-router-dom';

class QuoteLarge extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          quote: {}
        }
      }
    
      componentDidMount() {
        axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/174')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              quote: content
            })
            console.log(res.data.acf);
          })
          .catch(err => {
            console.log(err);
          })
      }

    render() {
        return(
            <section className="quoteCtaLarge">
                <div className="quoteContent">
                    <h2>{this.state.quote.cta_heading}</h2>
                    <p>{this.state.quote.cta_description_text}</p>
                    <Link to={"/contact"}><button className="secondary-btn">Get In Touch!</button></Link>
                </div>
            </section>
        );
    }
}

export default QuoteLarge; 