import React, {Component} from 'react';
import '../styles/Footer.scss';
import SubscribeInput from './SubscribeInput';

import logoWhite from '../images/sterlink-logo-white.svg';

class Footer extends Component {
    render() {
        return(
            <div className="footerBg">
                <div className="container">
                <footer className="footer">
                    <a href="/" className="logo"><img src={logoWhite} alt=""/></a>
                    <nav className="footerColumn">
                        <ul>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/contact">Contact</a></li>
                            <li><a>Terms &amp; Conditions</a></li>
                        </ul>
                    </nav>

                    <nav className="footerColumn">
                        <ul>
                            <li><a href="https://www.instagram.com/sterlinkprints/">Instagram</a></li>
                            <li><a href="https://www.twitter.com/sterlinkp">Twitter</a></li>
                            <li><a href="https://www.facebook.com/Sterlinkscreenprinting">Facebook</a></li>
                            <li><a href="https://www.linkedin.com/in/sterlink-prints-3b5aa4174/">LinkedIn</a></li>
                            <li><a href="https://www.pinterest.com/sterlinkprints">Pinterest</a></li>
                        </ul>
                    </nav>
                    
                    <div className="footerColumn">
                        <p>Subscribe to Our Newsletter</p>
                        <SubscribeInput />
                    </div>

                    <div className="footerColumn">
                        <p>    
                            16719 110th Ave E Puyallup WA 98374 <br />
                            253.466.8280 <br />
                            sterlinkprints@gmail.com
                        </p>
                    </div>
                </footer>
                </div>
            </div>    
        );
    }
}

export default Footer;