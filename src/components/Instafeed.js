import React from 'react';
import Instafeed from 'react-instafeed';

import '../styles/Instagram.scss';

class InstafeedLayout extends React.Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const instafeedTarget = 'instafeed';

    return (
        <div className="instagram">
            <h3 className="sectionTitle">
                FOLLOW US @STERLINKPRINTS
            </h3>
            <div className="instagramPosts" id={instafeedTarget}>
                <Instafeed
                limit='3'
                ref='instafeed'
                resolution='standard_resolution'
                sortBy='most-recent'
                target={instafeedTarget}
                template='<div class="post"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>'
                userId='4262934626'
                clientId='4abca2b0e9a14aab9b8017f632cec1d2'
                accessToken='4262934626.1677ed0.95ce878953bc442984edcac89aef10b9'
                />
            </div>
        </div>
      );
    }
  }

  export default InstafeedLayout;

  