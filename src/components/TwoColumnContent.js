import React, { Component } from 'react';
import '../styles/Garments.scss';

import sanmarLogo from '../images/sanmar-logo.png';

class TwoColumnContent extends Component {
    render() {
    return (
        <section className="container">
            <div className="content-container">
                <div className="image-box">
                    <div className="image-box-content">
                        <h5 className="brand-box-tagline">Sanmar - Where we order our garments from</h5>
                        <button><a href="https://www.sanmar.com">+</a></button>
                    </div>
                </div>
                <div className="content-box">
                    <div className="content-box-content">
                        <h5>Order Blank Shirts from Sanmar</h5>
                        <h2>Sanmar</h2>
                        <p>
                            {this.props.description}
                        </p>
                        <img className="partner-logo" src={sanmarLogo} alt=""/>
                    </div>
                </div>
            </div>
        </section>
      );
    }
  }
  
  export default TwoColumnContent;