import React, { Component } from 'react';
import '../styles/Garments.scss';
import axios from 'axios';

// Components
import Navigation from './Navigation';
import HeroSmall from './HeroSmall';
import TwoColumnContent from './TwoColumnContent';
import TwoColumnContentRev from './TwoColumnContentRev';
import Carousel from './Carousel';
import FullWidth from './FullWidth';
// import QuoteLarge from './QuoteLarge';
import FeaturedBrands from './FeaturedBrands';
import QuoteWhite from './QuoteWhite';
import Footer from './Footer';
import Loader from './Loader';

class Garments extends Component {
    constructor(props) {
      super(props);

      this.state = {
        blank_apparel: {}
      }
    }

    componentDidMount() {
      axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/219')
      .then(res => {
        const content = res.data.acf;
        this.setState({
          blank_apparel: content
        })
        console.log(res.data.acf);
      })
      .catch(err => {
        console.log(err);
      });
    }

    render() {
      return (
        <React.Fragment>
          {
            Object.keys(this.state.blank_apparel).length === 0 ?
            <Loader /> :
            <div>
              <Navigation />
              <HeroSmall title={this.state.blank_apparel.page_title}>{this.state.blank_apparel.page_description}</HeroSmall>
              <TwoColumnContent description={this.state.blank_apparel.sanmar_description} />
              <TwoColumnContentRev description={this.state.blank_apparel.alphabroder_description} />
              <FullWidth>{this.state.blank_apparel.page_content}</FullWidth>
              <Carousel />
              <FeaturedBrands />
              <QuoteWhite />
              <Footer />
            </div>
          }
        </React.Fragment>
      );
    }
  }
  
  export default Garments;