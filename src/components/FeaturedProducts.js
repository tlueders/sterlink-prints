import React, {Component} from 'react';
import '../styles/FeaturedProducts.scss';
import tshirt from '../images/t-shirt.jpg';

class FeaturedProducts extends Component {
    render() {
        return(
            <React.Fragment>
            <h3 className="sectionTitle">Featured Products</h3>
            <section className="featuredProducts">
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a href="#" className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a href="#" className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a href="#" className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
            </section>
            </React.Fragment> 
        );
    }
}

export default FeaturedProducts;