import React, {Component} from 'react';
import '../styles/QuoteWhite.scss';

import {Link} from 'react-router-dom';

class QuoteLarge extends Component {
    render() {
        return(
            <div className="container">
                <section className="quoteCtaWhite">
                    <div className="quoteContent">
                        <h2>Get A Quote Today</h2>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>
                        <Link to={"/contact"}><button className="secondary-btn">Get In Touch!</button></Link>
                    </div>
                </section>
            </div>
        );
    }
}

export default QuoteLarge; 