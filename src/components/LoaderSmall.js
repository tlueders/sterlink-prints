import React, { Component } from 'react';
import '../styles/Loader.scss';

class LoaderSmall extends Component {
    render() {
        return(
            <div className="loader-container">
                <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        );
    }
}

export default LoaderSmall;