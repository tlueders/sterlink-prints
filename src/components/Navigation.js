import React, { Component } from 'react';
import '../styles/Navigation.scss';
import logo from '../images/sterlink-logo.svg';
import { slide as Menu } from 'react-burger-menu';
import { Link } from "react-router-dom";


class Navigation extends Component {
    
  showSettings (event) {
    event.preventDefault();
  }

    render() {
      return (
        <div className="header-background">
          <div className="container">
            <nav className="mainNav">
                    <a href="/" className="logo"><img src={logo} alt=""/></a>
                    <ul className="navbar">
                        <li className="navlink"><Link to={"/services"}>Services</Link></li>
                        <li className="navlink"><Link to={"/garments"}>Blank Apparel</Link></li>
                        <li className="navlink"><Link to={"/contact"}>Contact</Link></li>
                        <li className="navlink"><Link to={"/quote"}>Get a Quote</Link></li>

                        {/* <li className="navlink snipcart-user-profile"><Link to={"#"}>Login</Link></li> */}
                        <li className="menu-item"><a href="http://shop.sterlinkprints.com">Shop</a></li> 
                    </ul>
            </nav>

              <Menu right={true} id={ "mobile-nav" }>
                <li className="menu-item"><Link to={"/services"}>Services</Link></li>
                <li className="menu-item"><Link to={"/garments"}>Blank Apparel</Link></li>
                <li className="menu-item"><Link to={"/contact"}>Contact</Link></li>
                <li className="menu-item"><Link to={"/quote"}>Get a Quote</Link></li>
                {/* <li className="menu-item snipcart-user-profile"><Link to={"#"}>Login</Link></li> */}
                <li className="menu-item"><a href="http://shop.sterlinkprints.com">Shop</a></li> 
              </Menu>
            </div>
        </div>
      );
    }
  }
  
  export default Navigation;