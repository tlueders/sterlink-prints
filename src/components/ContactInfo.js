import React, { Component } from 'react';
import '../styles/ContactInfo.scss';

class App extends Component {
  render() {
    return (
        <div className="contact-info">
            <div className="contact-content">
                <p>We understand forms are a lot of work!</p>
                <h2>Give Us A Call!</h2>
                <hr/>
                <ul>
                    <li>Office: <a href="tel:2534668280">253.466.8280</a></li>
                    <li>Email: <a href="emailto: sterlinkprints@gmail.com">sterlinkprints@gmail.com</a></li>
                    <li>Hours: Monday - Friday: 9am - 5pm</li>
                    <li>Address: 16719 E 110TH Avenue, Puyallup, WA 98374</li>
                </ul>
            </div>
        </div>
    );
  }
}

export default App;
