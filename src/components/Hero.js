import React from 'react';
import '../styles/Hero.scss';
import ReactPlayer from 'react-player'
import video from '../videos/sterlink-hero-video.mp4';

// const video = 'https://vimeo.com/298114768/16882cca47';

const Hero = (props) => (
    <section className="heroSection">
        <div className="container">
            {/* <div className="heroContent">
                <h1>{props.heroTitle}</h1>
                <p>{props.heroCaption}</p>
            </div> */}
            {/* <nav className="heroNav">
                <ul className="heroLinks">
                    <li className="heroSocial"><a href="https://www.instagram.com">Instagram</a></li>
                    <li className="heroSocial"><a href="https://www.twitter.com">Twitter</a></li>
                    <li className="heroSocial"><a href="https://www.facebook.com">Facebook</a></li>
                    <li className="heroCta"><a href="/services" >See Our Services ></a></li>
                </ul>
            </nav> */}
        </div>
        {/* <div className="overlay"></div> */}
        <ReactPlayer 
            url={video} 
            id="video-background"
            playing 
            loop
            muted 
            width='auto'
            height='auto'
        />  
    </section>
    )

export default Hero;