import React from 'react';
import '../styles/Form.scss';
import '../styles/SuccessMessage.scss';

import SuccessMessage from '../components/SuccessMessage';

//   BEGIN FORM

const encode = (data) => {
    return Object.keys(data)
        .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
        .join("&");
  }

  class Form extends React.Component {
    constructor(props) {
      super(props);
      this.state = { name: "", email: "", message: "", success: false };

      this.close = this.close.bind(this);
    }

    /* Here’s the juicy bit for posting the form submission */

    handleSubmit = e => {
      fetch("/", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: encode({ "form-name": "contact-form", ...this.state })
      })
        .then(() => this.success())
        .catch(error => alert(error));

        e.preventDefault();
        
        this.setState({
            name: "", email: "", message: ""
        });
      
    };
    close() {
        this.setState(state => ({
            success: false
        }));
    }

    success() {
        this.setState(state => ({
           success: true
        }));
         
    }

    handleChange = e => this.setState({ [e.target.name]: e.target.value });

    render() {
      const { name, email, message } = this.state;
      return (
          <div className="form-container">
          {this.state.success && <SuccessMessage close={this.close}/>}
            <form onSubmit={this.handleSubmit} ref="form">
            <p>
                <input type="text" name="name" placeholder="Name" value={name} onChange={this.handleChange} />
            </p>
            <p>
                <input type="email" name="email" placeholder="Email" value={email} onChange={this.handleChange} />
            </p>
            <p>
                <textarea name="message" value={message} placeholder="Description" onChange={this.handleChange} />
            </p>
            <p>
                <button type="submit">Send</button>
            </p>
            </form>
        </div>
      );
    }
  }

  export default Form;

  