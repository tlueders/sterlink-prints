import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./Home";
import Services from "./Services";
import Garments from "./Garments";
import Contact from "./Contact";
// import Apparel from "./Apparel";
import Quote from "./Quote";

import Printing from './Printing';
import Embroidery from './Embroidery';
import Balls from './Balls';

const Router = () => (
  <BrowserRouter>
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/services" component={Services} />
        <Route exact path="/printing" component={Printing} />
        <Route exact path="/embroidery" component={Embroidery} />
        <Route path="/balls" component={Balls} />
        <Route path="/garments" component={Garments} />
        <Route path="/contact" component={Contact} />
        <Route path="/quote" component={Quote} />
      </div>
  </BrowserRouter>
);

export default Router;
