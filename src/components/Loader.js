import React, { Component } from 'react';
import logo from '../images/sterlink-logo.svg';
import '../styles/Loader.scss';

class Loader extends Component {
    render() {
        return(
            <div className="loader-container">
                <img src={logo} className="loader-logo" alt=""/>
                <p>Loading </p>
                <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        );
    }
}

export default Loader;