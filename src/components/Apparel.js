import React, { Component } from 'react';
import '../App.scss';
import '../styles/FeaturedProducts.scss';
import tshirt from '../images/t-shirt.jpg';
import productData from '../data/products.json';
import Navigation from './Navigation';
import Footer from './Footer';

class Apparel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            products: []
        }
    }

    componentDidMount() {
        this.fetchProducts().then(this.setProducts);
    }

    fetchProducts = () => this.client.getEntries()   

    setProducts = response => {
        this.setState({
            products: response.items
        })
    }

  render() {
    return (
        <React.Fragment>
        <Navigation />
        <div className="container">
            <section className="featuredProducts">
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
                <div className="singleProduct">
                    <img src={tshirt} alt=""/>
                    <br />
                    <a className="productName">Product Name</a>
                    <p className="productPrice">$25.00</p>
                </div>
            </section>

            { this.state.products.map(({fields}, i) =>
                <pre key={i}>{JSON.stringify(fields, null, 2)}</pre>
            )}

            {productData.map((productDetail, index) => {
                return <a 
                            class="snipcart-add-item"
                            data-item-id={productDetail.id}
                            data-item-name={productDetail.name}
                            data-item-price={productDetail.price}
                            data-item-url={productDetail.url}
                        >
                        Add To Cart
                        </a>
            })}

        </div>
        <Footer />
        </React.Fragment> 

        
        
        
    );
  }
}

export default Apparel;
