import React, {Component} from 'react';
import Navigation from './Navigation';
import HeroSmall from './HeroSmall';
import QuoteForm from './QuoteForm';
import Footer from './Footer';

class Quote extends Component {
    render(){
        return(
            <div>
                <Navigation />
                <HeroSmall title="Get a Quote!">Interested in working together? Tell us more about your project!</HeroSmall>
                <div className="container">
                    <QuoteForm />
                </div>
                <Footer />    
            </div>    
        );
    }
}

export default Quote;