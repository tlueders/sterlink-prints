import React, {Component} from 'react';
import '../styles/Reviews.scss';

import payItForward from '../images/pay-it-forward.svg';
import recycle from '../images/recycle.svg';
import enviromental from '../images/enviromentally-friendly.svg';

class Reviews extends Component {
    render() {
        return(
            <section className="reviews">
                <div className="container reviewsContent">
                    <h3 className="sectionTitle">{this.props.title}</h3>
                    <p>{this.props.about_description}</p>

                    <div className="sectionContent">
                    <ul className="sectionContent-List">
                        
                        <li>
                            <img src={enviromental} alt="Enviromentally Friendly"/>
                            <label>{this.props.icon_title_one}</label>
                            <p>
                                {this.props.icon_text_one}
                            </p>
                        </li>
                    
                        <li>   
                            <img src={recycle} alt="Recycling"/>
                            <label>{this.props.icon_title_two}</label>
                            <p>
                                {this.props.icon_text_two}
                            </p>
                        </li>
                    
                        <li>
                            <img src={payItForward} alt="Paying it forward"/>
                            <label>{this.props.icon_title_three}</label>
                            <p>
                            {this.props.icon_text_three}
                            </p>
                        </li>
                    </ul>
                </div>
                </div>
            </section>
        );
    }
}

export default Reviews;