import React from 'react';
import '../styles/SuccessMessage.scss';

import { Link } from "react-router-dom";

class SuccessMessage extends React.Component {
    render() {
      return (
        <div className="successMessage">
            <div className="message-container">
                <button className="close-btn" onClick={this.props.close}><i>close</i></button>
                <h1>We will contact you shortly</h1>
                <p>In the mean time, please take a look through our apparrel shop!</p>
                <button className="primary-btn"><Link to="/shop">Shop</Link></button>
            </div>
        </div>
      );
    }
  }

  export default SuccessMessage;

  