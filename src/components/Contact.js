import React, { Component } from 'react';
import '../App.scss';

// Components
import Navigation from './Navigation';
import Footer from './Footer';
import HeroSmall from './HeroSmall';
import Form from './Form';
import ContactInfo from './ContactInfo';
import SubscribeLarge from './SubscribeLarge';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Navigation />
            <HeroSmall title="Contact">Get in touch! We'd love to work with you!</HeroSmall>
            <div className="container">
              <div className="contact-section">
                <Form />
                <ContactInfo />
              </div>
              <SubscribeLarge />
            </div>
          <Footer />
      </div>
    );
  }
}

export default App;
