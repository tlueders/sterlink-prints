import React from 'react';
import '../styles/QuoteForm.scss';
import '../styles/SuccessMessage.scss';

import SuccessMessage from './SuccessMessage';

//   BEGIN FORM

const encode = (data) => {
    return Object.keys(data)
        .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
        .join("&");
  }

  class QuoteForm extends React.Component {
    constructor(props) {
      super(props);

      this.state = { 
        name: "", 
        location: "",
        email: "",
        color: "1",
        businessName: "",
        productQty: "1-10", 
        message: "",
        upload: "",
        success: false 
    };

        this.close = this.close.bind(this);
        this.uploadField = this.uploadField.bind(this);
    }

    /* Here’s the juicy bit for posting the form submission */


    handleSubmit = e => {
      fetch("/", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: encode({ "form-name": "quote-form", ...this.state })
      })
        .then(() => this.success())
        .catch(error => alert(error));

        e.preventDefault();
        
        this.setState({
            name: "", 
            location: "",
            email: "",
            color: "",
            businessName: "",
            productQty: "", 
            message: "",
        });
      
    };

    close() {
        this.setState(state => ({
            success: false
        }));
    }

    success() {
        this.setState(state => ({
           success: true
        }));
         
    }

    uploadField(event) {
        if(event.target.files[0].size > 2200000){
            alert("File is too big!");
            this.setState({ [event.target.name]: "" })
        }else {
            this.setState({ [event.target.name]: event.target.value })
        };
    }

    handleChange = e => this.setState({ [e.target.name]: e.target.value });

    render() {
      const { name, location, email, color, businessName, productQty, message } = this.state;
      return (
          <div className="quoteform-container">
          {this.state.success && <SuccessMessage close={this.close}/>}
            <form onSubmit={this.handleSubmit} ref="form">
                <div className="form-inputs">
                    <p>
                        <input type="text" name="name" placeholder="Name" value={name} onChange={this.handleChange} />
                    </p>
                    <p>
                        <input type="text" name="location" placeholder="Location" value={location} onChange={this.handleChange} />
                    </p>
                </div>

                <div className="form-inputs">
                    <p>
                        <input type="text" name="email" placeholder="Email" value={email} onChange={this.handleChange} />
                    </p>
                    <p>
                        <select type="select" name="color" placeholder="Amount of Colors" value={color} onChange={this.handleChange}> 
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </p>
                </div>

                <div className="form-inputs">
                    <p>
                        <input type="text" name="businessName" placeholder="Business Name" value={businessName} onChange={this.handleChange} />
                    </p>
                    <p>
                        <select type="select" name="productQty" placeholder="Product Qty" value={productQty} onChange={this.handleChange}> 
                            <option>1-10</option>
                            <option>11-25</option>
                            <option>26-50</option>
                            <option>51-100</option>
                        </select>
                    </p>
                </div>

                <p>
                    <textarea name="message" value={message} placeholder="Description" onChange={this.handleChange} />
                </p>
                {/* <p>
                    <input name="upload" value={upload} type="file" onChange={this.uploadField}  id="file"/>
                </p> */}
                <p>
                    <button type="submit">Send</button>
                </p>
            </form>
        </div>
      );
    }
  }

  export default QuoteForm;

  