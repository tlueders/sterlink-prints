import React, {Component} from 'react';
import '../App.scss';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import Navigation from './Navigation';
import Footer from './Footer';
import Loader from './Loader';

class Embroidery extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          services_embroidery: {}
        }
      }
    
      componentDidMount() {
        axios.get('https://shop.sterlinkprints.com/wp-json/acf/v3/pages/189')
          .then(res => {
            const content = res.data.acf;
            this.setState({
              services_embroidery: content
            })
            console.log(res.data.acf);
          })
          .catch(err => {
            console.log(err);
          })
      }  

    render() {
        return(
            <div>
              {
                Object.keys(this.state.services_embroidery).length === 0 ?
                <Loader /> :
                <div>
                  <Navigation />
                  <div className="container">
                      {ReactHtmlParser(this.state.services_embroidery.service_content)}
                  </div>
                  <Footer />
                </div>
              }
            </div>
        );
    }
}

export default Embroidery;