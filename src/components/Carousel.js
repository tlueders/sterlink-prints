import React, {Component} from 'react';
import '../styles/Carousel.scss';

import OptionsSlider from "./Slider";

class Carousel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showGood: true,
            showBetter: false,
            showBest: false,
            activeGood: false,
            activeBetter: false,
            activeBest: false
        };

        this.showGood = this.showGood.bind(this);
        this.showBetter = this.showBetter.bind(this);
        this.showBest = this.showBest.bind(this);
    }

    showGood() {
        this.setState(state => ({
            showGood: true,
            showBetter: false,
            showBest: false,
            activeGood: true,
            activeBest: false,
            activeBetter: false,
        }));
    }   
    showBetter() {
        this.setState(state => ({
            showGood: false,
            showBetter: true,
            showBest: false,
            activeBetter: true,
            activeGood: false,
            activeBest: false
        }));
    }   
    showBest() {
        this.setState(state => ({
            showGood: false,
            showBetter: false,
            showBest: true,
            activeBest: true,
            activeBetter: false,
            activeGood: false
        }));
    }    

    render() {
        return(
            <div className="container">
                {this.state.showGood && <OptionsSlider name="Good"/>}
                {this.state.showBetter && <OptionsSlider name="Better"/>}
                {this.state.showBest && <OptionsSlider name="Best"/>}

                <div className="slider-selector">
                    <a onClick={this.showGood} className={this.state.activeGood && 'activeGood'}>Good</a>
                    <a onClick={this.showBetter} className={this.state.activeBetter && 'activeBetter'}>Better</a>
                    <a onClick={this.showBest} className={this.state.activeBest && 'activeBest'}>Best</a>
                </div>
            </div>
        );
    }
}

export default Carousel; 